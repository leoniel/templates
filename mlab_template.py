import numpy as np
from mayavi import mlab
nx = 46
ny = 33
nz = 46
A = np.loadtxt("THETA_OUT3.txt",dtype="float")
B = np.loadtxt("PHI_OUT3.txt",dtype="float")
C = np.loadtxt("A_OUT3.txt",dtype="float").reshape((nx*ny*nz,15))

Mag = np.sum(C**2,1)
Dmag = np.sum(C[:,1:]**2,1)

nx = 46
ny = 33
nz = 46
outx = np.zeros((4,nx*ny*nz))
outy = np.zeros((4,nx*ny*nz))
outz = np.zeros((4,nx*ny*nz))
outdoo = np.zeros((4,nx*ny*nz))
outmag= np.zeros((4,nx*ny*nz))
ind=0
for i in range(nx):
    for j in range(ny):
        for k in range(nz):
            outdoo[:,ind]= np.array((i,j,k,Dmag[ind]/Mag[ind]))
            outmag[:,ind]= np.array((i,j,k,Mag[ind]))
            ind+=1
ind=0
for i in range(nx):
    for j in range(ny):
        for k in range(nz):
            outx[:,ind] = np.array((i,j,k,Dmag[ind]*np.sin(A[ind])*np.cos(B[ind])))
            outy[:,ind] = np.array((i,j,k,Dmag[ind]*np.sin(A[ind])*np.sin(B[ind])))
            outz[:,ind] = np.array((i,j,k,Dmag[ind]*np.cos(A[ind])))
          #  print(outx[:,ind])
            ind+=1


##np.savetxt("x.csv", outx, delimiter=",")
##np.savetxt("y.csv", outy, delimiter=",")
##np.savetxt("z.csv", outz, delimiter=",")
##np.savetxt("mag.csv", outmag, delimiter=",")
##np.savetxt("doo.csv", outdoo, delimiter=",")

#mlab.quiver3d(outx[3,:].reshape((nx,ny,nz)),outy[3,:].reshape((nx,ny,nz)),outz[3,:].reshape((nx,ny,nz)),scale_factor=0.01)
#src = mlab.pipeline.vector_field(outx[3,:].reshape((nx,ny,nz)),outy[3,:].reshape((nx,ny,nz)),outz[3,:].reshape((nx,ny,nz)))

#mlab.pipeline.vectors(src, mask_points=5, scale_factor=3.)
#mlab.points3d((Dmag[:]).reshape((nx,ny,nz)))
#mlab.points3d((Mag[:]).reshape((nx,ny,nz)))
#mlab.points3d((Dmag[:]/Mag[:]).reshape((nx,ny,nz)))
#mlab.points3d((Dmag[:]/Mag[:]).reshape((nx,ny,nz)),mask_points=5,opacity=0.1)

mlab.points3d(outx[0,:],outx[1,:],outx[2,:],Dmag[:])

mlab.pipeline.volume(mlab.pipeline.scalar_field(Mag.reshape((nx,ny,nz))), vmin=0, vmax=1)
mlab.outline()
mlab.show()
